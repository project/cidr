## CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

## INTRODUCTION

CIDR-Classless Inter-Domain Routing

CIDR notation is a compact representation of an IP address and its associated
routing prefix. The notation is constructed from an IP address, a slash ('/')
character, and a decimal number. [...] The address may denote a single,
distinct interface address, or it may be the beginning address of an entire
network.

Cited from Classless Inter-Domain Routing on Wikipedia
https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation

* For a full description of this module, visit the project page:
  https://www.drupal.org/project/cidr

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/cidr


## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

## CONFIGURATION

* Navigate to Admin > Structures > CIDRs
* Then Click on Add CIDRs.
* Fill the fields with your custom configurations then save.
* All CIDRs are exportable configuration and can be easily versioned and
  deployed.

## MAINTAINER

Current maintainer:

* Stefan Auditor (sanduhrs) - https://www.drupal.org/u/sanduhrs
