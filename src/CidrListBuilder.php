<?php

namespace Drupal\cidr;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of cidrs.
 */
final class CidrListBuilder extends ConfigEntityListBuilder {

  /**
   * The CIDR service.
   *
   * @var \Drupal\cidr\CidrServiceInterface
   */
  protected CidrServiceInterface $cidr;

  /**
   * The class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type object.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage object.
   * @param \Drupal\cidr\CidrInterface $cidr
   *   The CIDR service.
   */
  public function __construct(
      EntityTypeInterface $entity_type,
      EntityStorageInterface $storage,
      CidrServiceInterface $cidr
    ) {
    $this->entityTypeId = $entity_type->id();
    $this->storage = $storage;
    $this->entityType = $entity_type;
    $this->cidr = $cidr;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
      ContainerInterface $container,
      EntityTypeInterface $entity_type
    ) {
    return new self(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('cidr.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    $header['range_start'] = $this->t('Range start');
    $header['range_end'] = $this->t('Range end');
    $header['suffix'] = $this->t('Suffix');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $this->cidr->setIp($entity->get('ip_dotted'));
    $this->cidr->setSuffix($entity->get('suffix'));

    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['range_start'] = $this->cidr->getRangeStartIp();
    $row['range_end'] = $this->cidr->getRangeEndIp();
    $row['suffix'] = $this->cidr->getSuffix();
    return $row + parent::buildRow($entity);
  }

}
