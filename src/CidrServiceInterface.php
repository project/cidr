<?php

namespace Drupal\cidr;

/**
 * Cidr service interface.
 */
interface CidrServiceInterface {

  /**
   * The ip in dotted notation.
   *
   * @param string $notation
   *   The notation type to return the ip in.
   *
   * @return string|false
   *   The ip address at the start of the range, boolean false for failure.
   */
  public function getIp($notation = 'dotted'): string|false;

  /**
   * The ip in dotted notation.
   *
   * @param string $ip
   *   The ip address.
   *
   * @return self
   *   The CIDR service object.
   */
  public function setIp($ip): self;

  /**
   * Get the suffix.
   *
   * @return int
   *   The cidr mask suffix.
   */
  public function getSuffix(): int;

  /**
   * The suffix.
   *
   * @param int $suffix
   *   The cidr mask suffix.
   *
   * @return self
   *   The CIDR service object.
   */
  public function setSuffix($suffix): self;

  /**
   * Get ip address and range in cidr notation.
   *
   * @return string
   *   The ip and range in cidr notation.
   */
  public function getCidr(): string;

  /**
   * Get ip at the start of the range.
   *
   * @param string $notation
   *   'dotted': The ip in dotted notation, e.g. 192.0.34.166.
   *   'numeric': The ip in numeric notation, e.g. 3221234342.
   *
   * @return string|false
   *   The ip address at the start of the range, boolean false for failure.
   */
  public function getRangeStartIp($notation = 'dotted'): string|false;

  /**
   * Get ip at the end of the range.
   *
   * @param string $notation
   *   'dotted': The ip in dotted notation, e.g. 192.0.34.166.
   *   'numeric': The ip in numeric notation, e.g. 3221234342.
   *
   * @return string|false
   *   The ip address at the end of the range, boolean false for failure.
   */
  public function getRangeEndIp($notation = 'dotted'): string|false;

  /**
   * Check if cidr is authoritative for a given ip.
   *
   * @var string $ip
   *   An IP address, e.g. '192.168.0.1'
   *
   * @return bool
   *   Return true if cidr is authoritative, false otherwise.
   */
  public function isAuthoritative($ip): bool;

  /**
   * Get authoritative ranges.
   *
   * @var string $ip
   *   An IP address, e.g. '192.168.0.1'
   *
   * @return array
   *   An array of valid cidr ranges.
   */
  public function getAuthoritativeRanges($ip): array;

}
