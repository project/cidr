<?php

namespace Drupal\cidr;

use Drupal\cidr\Entity\Cidr;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Cidr Service.
 */
class CidrService implements CidrServiceInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The ip address in dotted notation.
   *
   * @var string
   */
  protected $ip;

  /**
   * The range suffix.
   *
   * @var int
   */
  protected $suffix = 32;

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  private $entityQuery;

  /**
   * Constructs a Cidr service object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      EntityTypeManager $entity_type_manager
  ) {
    $this->configFactory = $config_factory;
    $this->entityQuery = $entity_type_manager->getStorage('cidr')->getQuery();
  }

  /**
   * {@inheritdoc}
   */
  public function getIp($notation = 'dotted'): string|false {
    return $this->getRangeStartIp($notation);
  }

  /**
   * {@inheritdoc}
   */
  public function setIp($ip): self {
    $this->ip = $ip;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSuffix(): int {
    return $this->suffix;
  }

  /**
   * {@inheritdoc}
   */
  public function setSuffix($suffix): self {
    $this->suffix = $suffix;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCidr(): string {
    return "{$this->ip}/{$this->suffix}";
  }

  /**
   * {@inheritdoc}
   */
  public function getRangeStartIp($notation = 'dotted'): string|false {
    switch ($notation) {
      case 'dotted':
        return $this->ip;

      case 'numeric':
        return sprintf("%u", ip2long($this->ip));
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRangeEndIp($notation = 'dotted'): string|false {
    $exp = 32 - $this->suffix;
    $numeric_ip = sprintf("%u", (ip2long($this->ip) + ($exp > 0 ? pow(2, $exp) : 0)));

    switch ($notation) {
      case 'dotted':
        return long2ip($numeric_ip);

      case 'numeric':
        return $numeric_ip;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isAuthoritative($ip): bool {
    return (bool) $this->getAuthoritativeRanges($ip);
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthoritativeRanges($ip): array {
    $numeric_ip = sprintf("%u", ip2long($ip));
    $this->entityQuery
      ->condition('status', 1)
      ->condition('range_start', $numeric_ip, '<=')
      ->condition('range_end', $numeric_ip, '>=');
    $entity_ids = $this->entityQuery->execute();
    return Cidr::loadMultiple($entity_ids);
  }

}
