<?php

namespace Drupal\cidr\PageCache;

use Drupal\cidr\CidrService;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cache policy for pages served from CIDR.
 *
 * This policy disallows caching of requests that use CIDR for security
 * reasons. Otherwise responses for authenticated requests can get into the
 * page cache and could be delivered to unprivileged users.
 */
class DisallowCidrRequests implements RequestPolicyInterface {

  /**
   * The CIDR service.
   *
   * @var \Drupal\cidr\CidrService
   */
  protected $cidr;

  /**
   * DisallowCidrRequests constructor.
   *
   * @param \Drupal\cidr\CidrService $cidr
   *   The Cidr service.
   */
  public function __construct(CidrService $cidr) {
    $this->cidr = $cidr;
  }

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {
    if ($this->cidr->isAuthoritative($request->getClientIp())) {
      return self::DENY;
    }
  }

}
